<?php 

session_start();

require 'credentials.php';

function post_exist($var){
    foreach($var as $clef){
        if(!isset($_POST[$clef]) || $_POST[$clef] == ''){
            return false; 
        }
    }
    return true; 
}


if(post_exist(['Nom','Prenom'])){
    $_nom = $_POST['Nom'];
    $_prenom = $_POST['Prenom'];
    $_id = $_POST['id'];

}
else{
    // header('Location: /formulaire.php')
    die('La page ne peux pas se charger !');
}

try{
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
    $stmt = $dbh->prepare("UPDATE utilisateur SET id=:id, Nom=:nom, Prenom=:prenom");
    $stmt->bindParam(':nom', $_nom);
    $stmt->bindParam(':prenom', $_prenom);
    $stmt->bindParam(':id', $_id);
    $stmt->execute();
    $resultats = $stmt -> fetchall();

    if(!$resultats){
        die('Erreur');
    }

}

catch(Exception $e){
     var_dump($e);
 }

if( $_nom = $_SESSION['Nom']  || $_prenom = $_SESSION['Prenom']){
    header('Location: /update.php');
}
?> 