<?php
    require 'credentialsBis.php';

   try{
       $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
       $resultats = $dbh->query("SELECT * FROM utilisateur");
       
   }

   catch(Exception $e){
        var_dump($e);
    }
?>

<!DOCTYPE html>
<html>

<?php  
require 'header.php'; 
?>

<body> 
    <h1> Utilisateurs</h1>
    <table>
        <thead>
            <tr>
                <th> Nom </th>
                <th> Prenom </th>
                <th> Nom de la rue </th>
                <th> Numéro de rue </th>
                <th> Code Postale </th>
                <th> Ville </th>
                <th> Email </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($resultats as $resultat): ?>
                <tr>
                    <td> <?php echo $resultat['Nom']; ?>     </td>
                    <td> <?php echo $resultat['Prenom']; ?>  </td>
                    <td> <?php echo $resultat['Nom_rue']; ?> </td>
                    <td> <?php echo $resultat['Num_rue']; ?> </td>
                    <td> <?php echo $resultat['Cp']; ?>      </td>
                    <td> <?php echo $resultat['Ville']; ?>   </td>
                    <td> <?php echo $resultat['Email']; ?>   </td>
                    <td> 
                        <form action ="delete.php" method="POST"> 
                        <input type="hidden"  name="id" value= <?php echo $resultat['id']; ?> > 
                        <input type ="submit">
                    </td>
                </tr>
            <?php endforeach; ?> 
        </tbody>
    </table>
</body>

</html>