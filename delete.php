<!DOCTYPE html>
<html>

<?php 
require 'header.php'; 

require 'credentials.php';

function post_exist($var){
    foreach($var as $clef){
        if(!isset($_POST[$clef]) || $_POST[$clef] == ''){
            return false; 
        }
    }
    return true; 
}

if(post_exist(['id'])){
    $_id = $_POST['id'];
    
}
else{
    die('Aucun paramètre fourni !');
}


try{
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
    $stmt = $dbh->prepare("DELETE FROM utilisateur WHERE id=:id");
    $stmt->bindParam(':id', $_id);
    if($stmt->execute()){
        header('Location: /formulaire/index.php');
    }
    else{ 
        echo 'erreur';
    }
    
}

catch(Exception $e){
     var_dump($e);
 }

?>


<body>

</body>

<?php require 'footer.php'; ?>
</html>