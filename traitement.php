<?php 

require 'credentials.php';

function post_exist($var){
    foreach($var as $clef){
        if(!isset($_POST[$clef]) || $_POST[$clef] == ''){
            return false; 
        }
    }
    return true; 
}

if(post_exist(['nom','prenom'])){
    $_nom = $_POST['nom'];
    $_prenom = $_POST['prenom'];

    $_total = $_nom." ".$_prenom;

}
else{
    // header('Location: /formulaire.php')
    die('La page ne peux pas se charger !');
}

try{
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
    $stmt = $dbh->prepare("INSERT INTO utilisateur (Nom, Prenom) VALUES (:nom, :prenom)");
    $stmt->bindParam(':nom', $_nom);
    $stmt->bindParam(':prenom', $_prenom);
    $stmt->execute();
    
}

catch(Exception $e){
     var_dump($e);
 }

?>


<!DOCTYPE html>
<html>

<?php  require 'header.php'; ?>

<body>
    <h1> bonjour <?php echo $_total ?> </h1>
</body>

<?php require 'footer.php'; ?>
</html>